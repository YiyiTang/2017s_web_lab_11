-- Answers to exercise 2 questions
DESC unidb_students;

-- A
SELECT unidb_students.fname AS fname, unidb_students.lname AS lname, CONCAT(unidb_attend.dept, unidb_attend.num) AS CN
      FROM unidb_students, unidb_attend WHERE unidb_attend.id = unidb_students.id AND CONCAT(unidb_attend.dept, unidb_attend.num)  = 'comp219';


-- B
SELECT unidb_students.fname, unidb_students.lname
    FROM unidb_students, unidb_courses
    WHERE unidb_students.id = unidb_courses.rep_id AND unidb_students.country != 'NZ';

-- C
SELECT unidb_lecturers.office,CONCAT(unidb_lecturers.fname, ' ',unidb_lecturers.lname) AS name
    FROM unidb_lecturers, unidb_courses
    WHERE unidb_lecturers.staff_no = unidb_courses.coord_no AND unidb_courses.num = '219';

-- D

SELECT CONCAT(unidb_students.fname, ' ', unidb_students.lname) AS name
FROM unidb_students, (SELECT unidb_attend.id AS id FROM unidb_attend, (SELECT unidb_teach.dept AS dept, unidb_teach.num AS num FROM unidb_teach, (SELECT unidb_lecturers.staff_no as staff_no FROM unidb_lecturers WHERE unidb_lecturers.fname = 'Te Taka') AS a
WHERE unidb_teach.staff_no = a.staff_no) AS b WHERE unidb_attend.dept = b.dept AND unidb_attend.num = b.num) AS c
  WHERE c.id = unidb_students.id;


SELECT CONCAT(unidb_students.fname, ' ', unidb_students.lname) AS name1
FROM (((unidb_students INNER JOIN unidb_attend ON unidb_students.id = unidb_attend.id)
            INNER JOIN unidb_teach ON unidb_teach.dept = unidb_attend.dept AND unidb_teach.num = unidb_attend.num))
        INNER JOIN unidb_lecturers ON unidb_teach.staff_no = unidb_lecturers.staff_no
WHERE unidb_lecturers.fname = 'Te Taka';


-- E


-- F
SELECT unidb_lecturers.fname, unidb_lecturers.lname FROM unidb_lecturers WHERE unidb_lecturers.office LIKE 'G%'
UNION
SELECT unidb_students.fname, unidb_students.lname FROM unidb_students WHERE unidb_students.country != 'NZ';


-- G

SELECT unidb_courses.coord_no, unidb_lecturers.fname, unidb_lecturers.lname FROM unidb_courses
    INNER JOIN unidb_lecturers ON unidb_courses.coord_no = unidb_lecturers.staff_no
    WHERE unidb_courses.dept = 'comp' AND unidb_courses.num = '219'

UNION
SELECT unidb_courses.rep_id, unidb_students.fname, unidb_students.lname FROM unidb_courses
    INNER JOIN unidb_students ON unidb_courses.rep_id = unidb_students.id
    WHERE unidb_courses.dept = 'comp' AND unidb_courses.num = '219';

